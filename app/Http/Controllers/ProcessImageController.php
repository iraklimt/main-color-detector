<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
use League\ColorExtractor\Palette;


class ProcessImageController extends Controller
{

    protected $given_colors = [
        'Aqua' => '00FFFF',
        'Black' => '000000',
        'Blue' => '0000FF',
        'Fuchsia' => 'FF00FF',
        'Gray' => '808080',
        'Green' => '008000',
        'Lime' => '00FF00',
        'Maroon' => '800000',
        'Navy' => '000080',
        'Olive' => '808000',
        'Purple' => '800080',
        'Red' => 'FF0000',
        'Silver' => 'C0C0C0',
        'Teal' => '008080',
        'White' => 'FFFFFF',
        'Yellow' => 'FFFF00',
    ];


    /**
     * @description Returns both predominant color and closest
     * color based on predominant and given colors
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMainImageColor(Request $request)
    {

        $request->validate([
            'file' => 'required|image',
        ]);

        $img = $request->file('file');

        $predominant_color = $this->getPredominantColor($img);

        $closest_color = $this->getClosestColor($predominant_color);


        return response()->json([
            'predominant_color' => $predominant_color,
            'closest_color' => $closest_color
        ]);
    }


    /**
     * @description This method uses the ColorExtractor Class to find
     * the image predominant color and returns it in hexadecimal format (#ffffff)
     *
     * @param $image
     * @return string
     */
    private function getPredominantColor($image)
    {
        $palette = Palette::fromFilename($image);

        $extractor = new ColorExtractor($palette);

        $main_color = $extractor->extract(1)[0];

        $main_hex_color = Color::fromIntToHex($main_color);

        return $this->parseHexToRgb($main_hex_color);
    }


    /**
     * @description This method parses hexadecimal color to rgb.
     * ie: #ffffff => [255,255,255]
     *
     * @param $colour
     * @return array|bool
     */
    private function parseHexToRgb($colour)
    {
            if ( $colour[0] == '#' ) {
                $colour = substr( $colour, 1 );
            }
            if ( strlen( $colour ) == 6 ) {
                list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
            } elseif ( strlen( $colour ) == 3 ) {
                list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
            } else {
                return false;
            }
            $r = hexdec( $r );
            $g = hexdec( $g );
            $b = hexdec( $b );

        return [$r,$g,$b];
    }


    /**
     * @description Calculates the deviation (absolute difference) between two rgb pairs
     *
     * @param $a
     * @param $b
     * @return number
     */
    protected function getDeviationBetweenTwoRgb($a, $b)
    {
        return abs($a[0] - $b[0]) + abs($a[1] - $b[1]) + abs($a[2] - $b[2]);
    }


    /**
     * @description This method calculates the closest rgb color
     *
     * @param $predominant_color
     * @return array
     */
    protected function getClosestColor($predominant_color)
    {
        $rgb_colors = [];

        // Parse all hex colors to rgb ( [r,g,b] )
        foreach($this->given_colors as $name => $hex){
            $rgb_colors[] = $this->parseHexToRgb($hex);
        }

        $selectedColor = $rgb_colors[0];
        $deviation = PHP_INT_MAX;

        // the smaller deviation will be the closest rgb color
        foreach ($rgb_colors as $color) {
            $curDev = $this->getDeviationBetweenTwoRgb($predominant_color, $color);
            if ($curDev < $deviation) {
                $deviation = $curDev;
                $selectedColor = $color;
            }
        }
        
        return $selectedColor;
    }

}