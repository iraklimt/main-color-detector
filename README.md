# Procedures

## Install laravel and create new Laravel project v6.x (from composer)
- `composer create-project --prefer-dist laravel/laravel main-color-detector`

## Create Vue scafolding (not set by default)
-  `composer require laravel/ui:^1.0 --dev`
-  `php artisan ui vue`
-  `npm install && npm run dev`
-  `php artisan preset non && php artisan preset vue`

## Install color-extractor composer package
- https://github.com/thephpleague/color-extractor
- `composer require league/color-extractor:0.3.*`

---

# Summary
- Approach:
  - A simple SPA where the user can upload any image and the app will guess which is the predominant color of the image.
  
- Arquitecture:
  - The app is a SPA based on Laravel endpoint (Server side) and VueJs frontend.
  - The client requests are handled by AJAX calls (using axios library).

- How it works:
  - Once the image is uploaded to the server, first we process the image (using a third party library "league/color-extractor") to get the predominant color based on pixel count.
  - The next step is parse the predominant color to RGB format to compare it against all 16 available colors (also parsed to RGB).
  - The comparison is made by calculating the absolute deviation between the predominant color and all the 16 available colors, where the smallest deviation will determine the closest color.
   