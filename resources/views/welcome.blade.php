<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Color-detector</title>

        <!-- CSRF -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="css/app.css">
    </head>
    <body>
        <div id="app">
            <!-- Here goes VUE -->
            <main-component></main-component>
        </div>

        <footer class="footer">
            <div class="content has-text-centered">
                <p>
                    <strong class="has-text-white">ColorChecker</strong> by <a href="https://gitlab.com/iraklimt/">Irakli M.</a>
                </p>
            </div>
        </footer>

        <!-- JS file -->
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
